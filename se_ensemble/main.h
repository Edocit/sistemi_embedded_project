/*
 * main.h
 *
 *  Created on: 09 mag 2020
 *      Author: gerry
 */

#ifndef MAIN_H_
#define MAIN_H_

#define BIT(x)	(1<<(x))


//HEX macro
#define SEG_9   111
#define SEG_8   127
#define SEG_7   39
#define SEG_6	125
#define SEG_5	109
#define SEG_4   102
#define SEG_3   79
#define SEG_2   91
#define SEG_1	6
#define SEG_0   63

#endif /* MAIN_H_ */
