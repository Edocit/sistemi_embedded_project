
#ifndef __3LNN_H__
#define __3LNN_H__


#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>

#define MNIST_IMG_WIDTH  28                               
#define MNIST_IMG_HEIGHT 28                                 
#define IN_NODES         784
#define HID_NODES        12
#define OUT_NODES        10

#define HID_W            784
#define OUT_W            12

typedef struct Network Network;
typedef struct hidNode hidNode;
typedef struct outNode outNode;

typedef enum LayerType {HIDDEN, OUTPUT} LayerType;

extern float in[MNIST_IMG_WIDTH * MNIST_IMG_HEIGHT];
extern float activations[OUT_NODES];
extern uint8_t pred;

extern const float wh[MNIST_IMG_WIDTH*MNIST_IMG_HEIGHT*HID_NODES];
extern const float wo[HID_NODES*OUT_NODES];
extern const float bh[HID_NODES];
extern const float bo[OUT_NODES];



struct hidNode{
    float bias;
    float output;
    float weights[HID_W];
};

struct outNode{
    float bias;
    float output;
    float weights[OUT_W];
};



struct Network{
    // i nodi di ingresso non mi servono, perchè il loro output è direttamente il pixel dell'immagine
    hidNode hid_node[HID_NODES];
    outNode out_node[OUT_NODES];
};


void activateNode(Network *nn, LayerType ltype, int id);
void calcNodeOutput(Network *nn, LayerType ltype, int id);
void feedForwardNetwork(Network *nn);
void getNetworkClassification(Network *nn);
void initWeight(Network *nn);
void predict(Network *nn, float *img);


#endif