#include "3lnn.h"
#include "neural_network.h"
#include "system.h"
#include "altera_avalon_pio_regs.h"
#include "main.h"

uint8_t lut[10] = {SEG_0,SEG_1,SEG_2,SEG_3,SEG_4,
				   SEG_5,SEG_6,SEG_7,SEG_8,SEG_9};

//VARIABILI GLOBALI DI RETE
float  input_image[MNIST_IMG_WIDTH * MNIST_IMG_HEIGHT]  __attribute__ ((section (".pesi"))) = {0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,49 ,180 ,253 ,255 ,253 ,169 ,36 ,11 ,76 ,9 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,5 ,68 ,228 ,252 ,252 ,253 ,252 ,252 ,160 ,189 ,253 ,92 ,0 ,0 ,0 ,0 ,0 ,0 ,0
                            ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,55 ,252 ,252 ,227 ,79 ,69 ,69 ,100 ,90 ,236 ,247 ,67 ,0 ,0 ,0 ,0 ,0 ,0 
                            ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,43 ,233 ,252 ,185 ,50 ,0 ,0 ,0 ,26 ,203 ,252 ,135 ,0 ,0 ,0 ,0 ,0 ,0 ,0 
                            ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,168 ,253 ,178 ,37 ,0 ,0 ,0 ,0 ,70 ,252 ,252 ,63 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,155 ,253 ,242 ,42 ,0 ,0 ,0 ,0 ,5 ,191 ,253 ,190 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,207 ,252 ,230 ,0 ,0 ,0 ,0 ,5 ,136 ,252 ,252 ,64 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 
                            ,0 ,0 ,0 ,0 ,0 ,0 ,207 ,252 ,230 ,0 ,0 ,0 ,32 ,138 ,252 ,252 ,227 ,16 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,165 ,252 ,249 ,207 ,207 ,207 ,228 ,253 ,252 ,252 ,160 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,9 ,179 ,253 ,252 ,252 ,252 ,252 ,75 ,169 ,252 ,56 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,64 ,116 ,116 ,74 ,0 ,149 ,253 ,215 ,21 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 
                            ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,253 ,252 ,162 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 
                            ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,32 ,253 ,240 ,50 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 
                            ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,157 ,253 ,164 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 
                            ,0 ,0 ,0 ,0 ,0 ,43 ,240 ,253 ,92 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 
                            ,0 ,0 ,93 ,253 ,252 ,84 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,114 ,252 ,209 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,207 ,252 ,116 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,165 ,252 ,116 ,0 ,0 
                            ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,93 ,200 ,63 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 };

//NETWORK 1
float in[MNIST_IMG_WIDTH * MNIST_IMG_HEIGHT];
float activations[OUT_NODES];
uint8_t pred;
//NETWORK 2
float    res[OUT_NODES];                             //uscita della rete in termini di probabilità (dopo la softmax)
uint8_t  actual_label;                        //etichetta predetta da paragonare a quella sopra per la backpropagation
//SHARED NETWORK 1 + NETWORK 2
uint8_t  ensemble_prediction;



void ensemble_avg(){
    uint8_t i;
    float max = 0;
    uint32_t reg_val = 0;
    uint8_t acc = 0;
    

	for(i = 0; i < OUTPUT_SHAPE; i++){
		//printf("net_1: %.4f   net_2: %.4f      ", activations[i], res[i]);
		res[i] = (res[i] + activations[i])/2.0;

		if(res[i] > max){
			max = res[i];
			ensemble_prediction = i;
		}
		//printf("%.4f\n", res[i]);
	}

	IOWR_ALTERA_AVALON_PIO_DATA(LEDS_BASE, BIT(ensemble_prediction));

	acc = (uint8_t)(res[ensemble_prediction]*100);

	reg_val  = lut[acc/10];
	reg_val  = reg_val << 8;
	reg_val |= lut[acc%10];

	IOWR_ALTERA_AVALON_PIO_DATA(HEX_BASE, reg_val);

	printf("Final ensemble prediction: %u | global accuracy: %u %%\n", ensemble_prediction, acc);

}






int main(){
    Network nn;

    initWeight(&nn);
    predict(&nn, input_image);
    net_2_prediction(weights_matrix, bias, input_image, res, &actual_label);
    ensemble_avg();
    while(1){}
    return 0;
}
