
#ifndef __NEURAL_NETWORK_H__
#define __NEURAL_NETWORK_H__

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdint.h>


//CARATTERISTICHE DELL' IMMAGINE (DEI DATI IN GENERALE)
#define IMAGE_WIDTH     28
#define IMAGE_HEIGHT    28
#define FLATTEN_IMAGE   ((IMAGE_WIDTH * IMAGE_HEIGHT))
#define INPUT_SHAPE     FLATTEN_IMAGE
#define OUTPUT_SHAPE    10
#define BIAS            10
#define LABELS          OUTPUT_SHAPE
#define MNIST_MAX_VAL   255.0


extern const float weights_matrix[OUTPUT_SHAPE][INPUT_SHAPE];   //matrice dei pesi 
extern const float bias[OUTPUT_SHAPE];                          //bias dello strato di output
extern float       res[10];                                     //uscita della rete in termini di probabilità (dopo la softmax)
extern uint8_t     actual_label;                                //etichetta predetta da paragonare a quella sopra per la backpropagation


//PROTOTYPES
//FUNZIONI DI CARICAMENTO E DI DUMP
void dump_weights_and_biases(const float (*weights_matrix)[INPUT_SHAPE], const float *bias);
//CICLO DI PREDIZIONE
void normalize_image(float *vectorized_image);
void affine_transformation(const float (*mtx)[INPUT_SHAPE], float *vectorized_image, const float *bias_vect, float *result);
void cumulative_output_softmax(float *activations);
void predict_out(float *probs, int *class);
//WRAPPER DELLE 4 FUNZIONI PRECEDENTI
void net_2_prediction(const float (*weights_mtx)[INPUT_SHAPE], const float *bias, float *input_image, float *result, uint8_t *predicted_label);



#endif